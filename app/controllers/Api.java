package controllers;

import models.*;
import play.Logger;
import play.api.mvc.Call;
import play.data.Form;
import play.libs.Json;

import play.mvc.Result;
import utils.AmountUtils;
import utils.ErrorUtils;

import javax.validation.ValidationException;

/**
 * Created by dalius on 22/10/2014.
 */
public class Api extends Base {

    public static Result documentation() {
       return ok(views.html.documentation.render());
    }

    public static Result fetchEngagement(String customer) {
        Logger.info("Engagement " + request().remoteAddress() + "/" + customer);
        response().setHeader("Access-Control-Allow-Origin", "*");

        try {
            EngagementTO engagement = fetchEngagementService(customer);
            return ok(Json.toJson(engagement));
        } catch (Exception e) {
            return ErrorUtils.handleJsonExecutionException(e);
        }

    }

    public static Result fetchTransactions(String customer, String number) {
        Logger.info("Transactions " + request().remoteAddress() + "/" + customer);

        response().setHeader("Access-Control-Allow-Origin", "*");
        try {
            AccountTO accountOverview = fetchTransactionsService(customer, number);
            return ok(Json.toJson(accountOverview));
        } catch (Exception e) {
            return ErrorUtils.handleJsonExecutionException(e);
        }
    }

    private static AccountTO fetchTransactionsService(String customer, String number) {
        Account account = loadAccount(number);

        if (!customer.equals(account.getCustomer().getNumber())) {
            throw new ValidationException("Account is not accessible.");
        }

        AccountTO accountOverview = AccountTO.map(account);

        Call next = routes.Api.transfer(customer);
        Call back = routes.Api.fetchEngagement(account.getCustomer().getNumber());

        accountOverview.getLinks().put("next", new LinkTO("POST", next.url()));
        accountOverview.getLinks().put("back", new LinkTO("GET", back.url()));
        return accountOverview;
    }

    public static Result transfer(String customer) {
        Logger.info("Transfer " + request().remoteAddress() + "/" + customer + " -> " + request().body().asJson().toString());


        response().setHeader("Access-Control-Allow-Origin", "*");
        response().setHeader("Access-Control-Allow-Headers", "*");

        try {
            TransferDetailsTO transferDetails = transferService(customer);
            return ok(Json.toJson(transferDetails));
        } catch (Exception e) {
            return ErrorUtils.handleJsonExecutionException(e);
        }

    }



    private static EngagementTO fetchEngagementService(String customer) {
        EngagementTO engagement = EngagementTO.map(loadCustomer(customer));

        for (AccountOverviewTO account : engagement.getAccounts()) {
            Call next = routes.Api.fetchTransactions(customer, account.getAccountNumber());
            account.getLinks().put("next", new LinkTO("GET", next.url()));
        }
        return engagement;
    }

    private static TransferDetailsTO transferService(String customer) {
        Form<TransferTO> form = Form.form(TransferTO.class).bind(request().body().asJson());

        if (form.hasErrors()) {
            throw new FormValidationException("Order contains errors", form);
        }

        TransferTO transfer = form.get();

        Account from = loadAccount(transfer.getFromAccount());
        Account to = loadAccount(transfer.getToAccount());

        if (!customer.equals(from.getCustomer().getNumber())) {
            throw new ValidationException("Account is not accessible.");
        }

        if (from.getNumber().equals(to.getNumber())) {
            throw new ValidationException("Incorrect To account");
        }

        final int amount = parseAmount(transfer.getAmount());

        if (AmountUtils.sum(from.getTransactions()) < amount) {
            throw new ValidationException("Not enough funds");

        }

        Transaction transaction = new Transaction(amount, to, from, transfer.getMessage());

        transaction.save();

        TransferDetailsTO transferDetails = TransferDetailsTO.map(transaction);

        return transferDetails;
    }


    public static Result transferOptions(String customer) {

        response().setHeader("Access-Control-Allow-Origin", "*");
        response().setHeader("Access-Control-Allow-Headers", "accept, content-type");
        return ok();
    }
}
