package controllers;

import com.avaje.ebean.Query;
import com.google.common.collect.Lists;
import models.Account;
import models.Customer;
import models.Transaction;
import play.db.ebean.Model;
import play.db.jpa.Transactional;
import play.mvc.Controller;

import javax.persistence.OrderBy;
import javax.validation.ValidationException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;
import java.util.function.IntBinaryOperator;
import java.util.stream.Collector;

/**
 * Created by dalius on 22/10/2014.
 */
public abstract class Base extends Controller {

    private static final AtomicInteger number = new AtomicInteger(600);

    @Transactional(readOnly = true)
    private static <T> Optional<T> loadModel(int id, Class<T> clazz) {
        return Optional.of(clazz.cast(new Model.Finder(Integer.class, clazz).byId(id)));
    }

    @Transactional(readOnly = true)
    private static <T> Optional<T> findByNumber(String number, Class<T> clazz) {
        Object result = new Model.Finder(Integer.class, clazz)
                .where().eq("number", number).findUnique();
        if (result == null) {
            return Optional.empty();
        }
        return Optional.of(clazz.cast(result));
    }


    @Transactional(readOnly = true)
    protected static Customer loadCustomer(String number) {
        Customer customer = findByNumber(number, Customer.class)
                .orElseThrow(() ->
                        new ValidationException("Customer " + number + " does not exist"));

        final List<Account> accounts = customer.getAccounts().parallelStream().map(
                (a) -> loadAccount(a.getNumber())
        ).collect(ArrayList::new, ArrayList::add, ArrayList::addAll);

        customer.setAccounts(accounts);

        return customer;
    }

    @Transactional(readOnly = true)
    protected static Account loadAccount(String number) {
        Account account = findByNumber(number, Account.class).orElseThrow(() ->
                new ValidationException("Account " + number + " does not exist"));

        List<Transaction> transactions = loadTransactions(account.getNumber());
        account.setTransactions(transactions);
        return account;
    }

    private static Query transactionsByNumber(String number) {
        return new Model.Finder(Integer.class, Transaction.class)
                .where(String.format("to_account = '%s' OR from_account = '%s' ORDER BY date DESC", number, number));
    }

    @Transactional(readOnly = true)
    protected static List<Transaction> loadTransactions(String accountNumber) {
        List<Transaction> transactions = transactionsByNumber(accountNumber).findList();

        for (Transaction tx : transactions) {
            if (tx.getFromAccount().equals(accountNumber)) {
                tx.setAmount(tx.getAmount()*-1);
            }
        }
        return transactions;
    }

    @Transactional(readOnly = true)
    protected static List<Transaction> loadTransactions() {
        return new Model.Finder(Integer.class, Transaction.class)
                .where()
                .setOrderBy("date DESC")
                .findList();
    }

    protected static String generateAccountNumber() {
        return new StringBuilder().append("LT2700").append(System.nanoTime()).toString();
    }

    protected static String generateCustomerNumber() {
        String time = String.format("%d",System.nanoTime());
        return number.incrementAndGet() + time.substring(time.length() - 3);
    }

    @Transactional
    protected static Account addAccount(Customer customer) {

        Account account = new Account(generateAccountNumber());
        customer.getAccounts().add(account);
        saveOrUpdate(customer);
        return loadAccount(account.getNumber());
    }

    @Transactional
    protected static Customer saveOrUpdate(final Customer customer) {

        if (customer.getId() != null) {
            customer.update();
        } else {
            customer.setNumber(generateCustomerNumber());
            customer.save();
        }

        return findByNumber(customer.getNumber(), Customer.class)
                .orElseThrow(() ->
                        new IllegalArgumentException("Customer " + customer.getNumber() + " does not exist"));
    }



    protected static Integer parseAmount(String amount) {
        if (amount == null || !amount.matches("^([0-9]+).([0-9]{2})$")) {
            throw new ValidationException("Amount must be 0.00 format and positive.");
        }

        return Double.valueOf(Double.valueOf(amount) * 100).intValue();
    }
}
