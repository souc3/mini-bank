package controllers;

import models.*;
import play.data.Form;
import play.db.ebean.Model;
import play.db.jpa.Transactional;
import play.mvc.*;

import javax.validation.ValidationException;
import java.util.*;

public class Management extends Base {

    @Transactional(readOnly = true)
    public static Result home() {
        List<Customer> customers = new Model.Finder(Integer.class, Customer.class).all();
        List<TransactionTO> transactions = loadTransactions().parallelStream()
                .map(TransactionTO::map)
                .collect(ArrayList::new, ArrayList::add, ArrayList::addAll);
        return ok(views.html.management.home.render("Your new application is ready.", customers, transactions));
    }

    public static Result viewCustomer(String number, String account) {

        Optional<Customer> customer = Optional.ofNullable(number)
                .map(Base::loadCustomer);

        List<Transaction> transactions = Optional.ofNullable(account)
                .map(Base::loadAccount)
                .map((a) -> a.getTransactions())
                .orElse(Collections.EMPTY_LIST);
        List<TransactionTO> tx = transactions.parallelStream()
                .map(TransactionTO::map)
                .collect(ArrayList::new, ArrayList::add, ArrayList::addAll);

        return ok(views.html.management.customer.render(customer.map(CustomerTO::map), tx));
    }


    public static Result modifyCustomer() {
        Form<Customer> form = Form.form(Customer.class).bindFromRequest();

        if (form.hasErrors()) {
            return badRequest(form.errorsAsJson());
        }

        Customer customer = saveOrUpdate(form.get());

        return redirect(routes.Management.viewCustomer(customer.getNumber(), null));
    }

    public static Result addAccount(final String number) {
        Form<AddAccount> form = Form.form(AddAccount.class).bindFromRequest();

        if (form.hasErrors()) {
            return badRequest(form.errorsAsJson());
        }

        Customer customer = loadCustomer(number);
        Account account = addAccount(customer);

        int amount = parseAmount(form.get().getAmount());

        Transaction tx = new Transaction(amount, account.getNumber(), "", "Free money");
        tx.setReceiverName(account.getCustomer().getName());
        tx.setSenderName("Bank");
        tx.save();

        return redirect(routes.Management.viewCustomer(customer.getNumber(), null));
    }

    @Transactional
    public static Result deleteCustomer(String number) {
        loadCustomer(number).delete();

        return redirect(routes.Management.home());
    }

    @Transactional
    public static Result transfer() {
        Form<TransferTO> form = Form.form(TransferTO.class).bindFromRequest();

        if (form.hasErrors()) {
            return badRequest(form.errorsAsJson());
        }

        TransferTO transfer = form.get();

        Account from = loadAccount(transfer.getFromAccount());
        Account to = loadAccount(transfer.getToAccount());

        if (from.getNumber().equals(to.getNumber())) {
            throw new ValidationException("From and To must differ!");
        }

        final int amount = parseAmount(transfer.getAmount());

        Transaction transaction = new Transaction(amount, to, from, transfer.getMessage());

        transaction.save();

        return redirect(routes.Management.viewCustomer(from.getCustomer().getNumber(), from.getNumber()));
    }

}
