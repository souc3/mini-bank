package utils;

import models.Account;
import models.AccountTO;
import models.Transaction;
import models.TransactionTO;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dalius on 27/10/2014.
 */
public class AmountUtils {

    public static int sum(List<Transaction> transactions) {
        return transactions.parallelStream().mapToInt((tx) -> tx.getAmount()).reduce(0, (x, y) -> x+y);
    }
}
