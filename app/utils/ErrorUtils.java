package utils;

import com.fasterxml.jackson.databind.JsonNode;
import models.FormValidationException;
import play.Logger;
import play.data.Form;
import play.libs.Json;
import play.mvc.Result;

import javax.validation.ValidationException;
import java.util.Objects;
import java.util.Optional;

import static play.mvc.Results.badRequest;
import static play.mvc.Results.internalServerError;

/**
 * Created by dalius on 27/10/2014.
 */
public class ErrorUtils {

    public static Result handleJsonExecutionException(Throwable throwable) {

        if (throwable instanceof FormValidationException) {

            Logger.error("Form validation: " + throwable.getMessage());
            return badRequest(JsonError.newValidation((FormValidationException) throwable));
        } else if (throwable instanceof ValidationException) {

            Logger.error("Validation: " + throwable.getMessage());
            return badRequest(JsonError.newValidation((ValidationException) throwable));
        } else {

            Logger.error("Internal: " + throwable.getMessage());
            return internalServerError(JsonError.newInetrnal(throwable));
        }
    }

    //TODO: rafactor this

    public static class JsonError {
        public final String type;
        public final String message;
        public final JsonNode data;

        private JsonError(String type, String message, Optional<JsonNode> data) {
            this.type = Objects.requireNonNull(type);
            this.message = Objects.requireNonNull(message);
            this.data = data.orElse(null);
        }

        private JsonError(String type, Throwable throwable, Optional<JsonNode> data) {
            this(type, Objects.requireNonNull(throwable).getLocalizedMessage(), data);
        }


        static JsonNode newValidation(FormValidationException exception) {
            return Json.toJson(new JsonError("validation", exception,
                    Optional.of(extractErrorData(exception.form))));
        }

        public static JsonNode newValidation(ValidationException exception) {
            return Json.toJson(new JsonError("validation", exception,
                    Optional.<JsonNode>empty()));
        }

        public static JsonNode newInetrnal(Throwable throwable) {
            return Json.toJson(new JsonError("internal", throwable,
                    Optional.<JsonNode>empty()));
        }

        public static JsonNode newHttp(String message) {
            return Json.toJson(new JsonError("http", message, Optional.<JsonNode>empty()));
        }

        static JsonNode extractErrorData(Form form) {
            return form.errorsAsJson();
        }
    }
}
