package models;

import play.data.validation.Constraints;

/**
 * Created by dalius on 22/10/2014.
 */
public class TransferTO {

    @Constraints.Required
    private String fromAccount;

    @Constraints.Required
    private String toAccount;

    @Constraints.Required
    @Constraints.Pattern(value = "^([0-9]+).([0-9]{2})$", message = "Amount must be 0.00 format and positive.")
    private String amount;

    @Constraints.Required
    private String message;

    public String getFromAccount() {
        return fromAccount;
    }

    public void setFromAccount(String fromAccount) {
        this.fromAccount = fromAccount;
    }

    public String getToAccount() {
        return toAccount;
    }

    public void setToAccount(String toAccount) {
        this.toAccount = toAccount;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
