package models;

import com.google.common.collect.Maps;

import java.util.List;
import java.util.Map;

/**
 * Created by dalius on 22/10/2014.
 */
public class AccountOverviewTO {
    private String accountNumber;
    private String balance;
    private Map<String, LinkTO> links = Maps.newHashMap();

    public AccountOverviewTO(String accountNumber, String balance) {
        this.accountNumber = accountNumber;
        this.balance = balance;
    }

    public static AccountOverviewTO map(final Account account) {
        final List<Transaction> transactions = account.getTransactions();
        int balance = transactions.parallelStream().mapToInt((tx) -> tx.getAmount()).reduce(0, (x, y) -> x+y);
        return new AccountOverviewTO(
                account.getNumber(),
                String.format("$ %.2f", (double) balance/100)
        );
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public Map<String, LinkTO> getLinks() {
        return links;
    }

    public void setLinks(Map<String, LinkTO> links) {
        this.links = links;
    }
}
