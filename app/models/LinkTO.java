package models;


/**
 * Created by dalius on 23/10/2014.
 */
public class LinkTO {

    private String method;
    private String uri;

    public LinkTO(String method, String uri) {
        this.method = method;
        this.uri = uri;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }
}
