package models;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dalius on 22/10/2014.
 */
public class CustomerTO {

    private String customerNumber;
    private String customerName;
    private List<AccountTO> accounts;

    public CustomerTO(String customerNumber, String customerName, List<AccountTO> accounts) {
        this.customerNumber = customerNumber;
        this.customerName = customerName;
        this.accounts = accounts;
    }

    public static CustomerTO map(Customer customer) {

        return new CustomerTO(
                customer.getNumber(),
                customer.getName(),
                customer.getAccounts().parallelStream()
                        .map(AccountTO::map)
                        .collect(ArrayList::new, ArrayList::add, ArrayList::addAll)
        );
    }

    public String getCustomerNumber() {
        return customerNumber;
    }

    public void setCustomerNumber(String customerNumber) {
        this.customerNumber = customerNumber;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public List<AccountTO> getAccounts() {
        return accounts;
    }

    public void setAccounts(List<AccountTO> accounts) {
        this.accounts = accounts;
    }
}
