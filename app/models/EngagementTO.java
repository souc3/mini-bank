package models;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dalius on 22/10/2014.
 */
public class EngagementTO {

    private String customerNumber;
    private String customerName;
    private List<AccountOverviewTO> accounts;

    public EngagementTO(String customerNumber, String customerName, List<AccountOverviewTO> accounts) {
        this.customerNumber = customerNumber;
        this.customerName = customerName;
        this.accounts = accounts;
    }

    public static EngagementTO map(Customer customer) {

        return new EngagementTO(
                customer.getNumber(),
                customer.getName(),
                customer.getAccounts().parallelStream()
                        .map(AccountOverviewTO::map)
                        .collect(ArrayList::new, ArrayList::add, ArrayList::addAll)
        );
    }

    public String getCustomerNumber() {
        return customerNumber;
    }

    public void setCustomerNumber(String customerNumber) {
        this.customerNumber = customerNumber;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public List<AccountOverviewTO> getAccounts() {
        return accounts;
    }

    public void setAccounts(List<AccountOverviewTO> accounts) {
        this.accounts = accounts;
    }
}
