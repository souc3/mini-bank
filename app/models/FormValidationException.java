package models;


import play.data.Form;

import javax.validation.ValidationException;
import java.util.Objects;

/**
 * Created by dalius on 09/09/2014.
 */
public class FormValidationException extends ValidationException {

    public final Form form;

    public FormValidationException(String message, Form form) {
        super(Objects.requireNonNull(message));
        this.form = Objects.requireNonNull(form);
    }

}
