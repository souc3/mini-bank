package models;

import com.google.common.collect.Maps;

import java.text.SimpleDateFormat;
import java.util.Map;

/**
 * Created by dalius on 23/10/2014.
 */
public class TransferDetailsTO extends TransactionTO {


    private Map<String, LinkTO> links = Maps.newHashMap();

    public TransferDetailsTO(String amount, String message, String date, String toAccount, String fromAccount, String senderName, String receiverName) {
        super(amount, message, date, toAccount, fromAccount, senderName, receiverName);
    }

    public static TransferDetailsTO map(Transaction tx) {
        return new TransferDetailsTO(
                String.format("$ %.2f", (double)tx.getAmount() / 100),
                tx.getMessage(),
                new SimpleDateFormat(DF).format(tx.getDate()),
                tx.getToAccount(),
                tx.getFromAccount(),
                tx.getSenderName(),
                tx.getReceiverName());
    }

    public Map<String, LinkTO> getLinks() {
        return links;
    }

    public void setLinks(Map<String, LinkTO> links) {
        this.links = links;
    }
}
