package models;

import javax.persistence.Column;
import java.text.SimpleDateFormat;

/**
 * Created by dalius on 22/10/2014.
 */
public class TransactionTO {

    protected static final String DF = "YYYY-MM-dd HH:mm:ss";

    private String amount;

    private String message;

    private String date;

    private String toAccount;

    private String fromAccount;

    private String senderName;

    private String receiverName;

    public TransactionTO(String amount, String message, String date, String toAccount, String fromAccount, String senderName, String receiverName) {
        this.amount = amount;
        this.message = message;
        this.date = date;
        this.toAccount = toAccount;
        this.fromAccount = fromAccount;
        this.senderName = senderName;
        this.receiverName = receiverName;
    }


    public static TransactionTO map(Transaction tx) {
        return new TransactionTO(
                String.format("$ %.2f", (double)tx.getAmount() / 100),
                tx.getMessage(),
                new SimpleDateFormat(DF).format(tx.getDate()),
                tx.getToAccount(),
                tx.getFromAccount(),
                tx.getSenderName(),
                tx.getReceiverName());
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getToAccount() {
        return toAccount;
    }

    public void setToAccount(String toAccount) {
        this.toAccount = toAccount;
    }

    public String getFromAccount() {
        return fromAccount;
    }

    public void setFromAccount(String fromAccount) {
        this.fromAccount = fromAccount;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public String getReceiverName() {
        return receiverName;
    }

    public void setReceiverName(String receiverName) {
        this.receiverName = receiverName;
    }
}
