package models;

import play.data.validation.Constraints;

/**
 * Created by dalius on 23/10/2014.
 */
public class AddAccount {

    @Constraints.Pattern(value = "^([0-9]+).([0-9]{2})$", message = "Amount must be 0.00 format and positive.")
    private String amount;

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }
}
