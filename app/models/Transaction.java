package models;

import play.db.ebean.Model;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by dalius on 22/10/2014.
 */

@Entity
public class Transaction extends Model {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(nullable = false)
    private Integer amount;

    @Column(nullable = false)
    private Date date;

    @Column(name = "to_account", nullable = false)
    private String toAccount;

    @Column(name = "from_account", nullable = false)
    private String fromAccount;

    @Column(nullable = false)
    private String senderName;

    @Column(nullable = false)
    private String receiverName;

    @Column(nullable = false)
    private String message;

    public Transaction (int amount, String toAccount, String fromAccount, String message) {
        this.amount = amount;
        this.date = new Date();
        this.message = message;
        this.toAccount = toAccount;
        this.fromAccount = fromAccount;
    }

    public Transaction (int amount, Account toAccount, Account fromAccount, String message) {
        this(amount, toAccount.getNumber(), fromAccount.getNumber(), message);
        senderName = fromAccount.getCustomer().getName();
        receiverName = toAccount.getCustomer().getName();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getToAccount() {
        return toAccount;
    }

    public void setToAccount(String toAccount) {
        this.toAccount = toAccount;
    }

    public String getFromAccount() {
        return fromAccount;
    }

    public void setFromAccount(String fromAccount) {
        this.fromAccount = fromAccount;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public String getReceiverName() {
        return receiverName;
    }

    public void setReceiverName(String receiverName) {
        this.receiverName = receiverName;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
