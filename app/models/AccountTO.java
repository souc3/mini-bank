package models;

import utils.AmountUtils;

import javax.persistence.CascadeType;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by dalius on 22/10/2014.
 */
public class AccountTO extends AccountOverviewTO {

    private List<TransactionTO> transactions;

    public AccountTO(String accountNumber, String balance, List<TransactionTO> transactions) {
        super(accountNumber, balance);
        this.transactions = transactions;
    }


    public static AccountTO map(final Account account) {
        final List<Transaction> transactions = account.getTransactions();
        int balance = AmountUtils.sum(transactions);
        return new AccountTO(
                account.getNumber(),
                String.format("$ %.2f", (double) balance/100),
                transactions.parallelStream()
                        .map(TransactionTO::map)
                        .collect(ArrayList::new, ArrayList::add, ArrayList::addAll)
        );
    }

    public List<TransactionTO> getTransactions() {
        return transactions;
    }

    public void setTransactions(List<TransactionTO> transactions) {
        this.transactions = transactions;
    }
}
