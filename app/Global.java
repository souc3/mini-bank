import com.google.common.net.HttpHeaders;
import play.Application;
import play.GlobalSettings;
import play.Logger;
import play.libs.F.Promise;
import play.libs.Json;
import play.mvc.Http.RequestHeader;
import play.mvc.Result;
import utils.ErrorUtils;

import static play.mvc.Results.badRequest;
import static play.mvc.Results.notFound;

/**
 * Created by dalius on 05/09/2014.
 */
public class Global extends GlobalSettings {

    public void onStart(Application app) {
        Logger.info("Mini-Bank app has started");
    }

    public void onStop(Application app) {
        Logger.info("Mini-Bank app shutdown...");
    }

    @Override
    public Promise<Result> onHandlerNotFound(RequestHeader requestHeader) {
        Logger.error("Handler not found");
        return Promise.pure(notFound(Json.toJson(ErrorUtils.JsonError.newHttp("404 Not found"))));
    }

    @Override
    public Promise<Result> onBadRequest(RequestHeader requestHeader, String s) {
        Logger.error("Bad request");
        return Promise.pure(badRequest(Json.toJson(ErrorUtils.JsonError.newHttp("403 Bad request"))));
    }


    public Promise<Result> onError(RequestHeader request, Throwable t) {
        Logger.debug(request.getHeader(HttpHeaders.ACCEPT));
        if (request.uri().startsWith("/api")) {
            Logger.debug("Handling API request error");
            Result result = ErrorUtils.handleJsonExecutionException(t);
            return Promise.<Result>pure(result);
        } else {
            Logger.debug("Handling general error " + t.getCause().getClass().getSimpleName());
            return super.onError(request, t);
        }

    }


}
