'use strict';

/**
 * @ngdoc overview
 * @name bankApp
 * @description
 * # bankApp
 *
 * Main module of the application.
 */
var app = angular
  .module('miniBankApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ]);

  app.config(['$routeProvider', '$httpProvider', function ($routeProvider, $httpProvider) {

    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .when('/:accountId', {
        templateUrl: 'views/account.html',
        controller: 'AccountCtrl'
      })
        //.when('/about', {
        //  templateUrl: 'views/about.html',
        //  controller: 'AboutCtrl'
        //})
      .otherwise({
        redirectTo: '/'
      });



    $httpProvider.defaults.useXDomain = true;
    delete $httpProvider.defaults.headers.common['X-Requested-With'];

  }]);
