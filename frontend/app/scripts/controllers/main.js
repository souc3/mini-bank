'use strict';

/**
 * @ngdoc function
 * @name miniBankApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the miniBankApp
 */
angular.module('miniBankApp')
    .controller('MainCtrl', ['$scope', '$cookieStore', '$http', '$route', function ($scope, $cookieStore, $http, $route) {
      $scope.customerId = $cookieStore.get('custId');
      $scope.showMessage = false;

      if ($scope.customerId) {
        $http.get('http://192.168.2.1:9000/api/engagement/'+ $scope.customerId)
            .success(function(data) {
              $scope.baseInfo = data;
              console.log(data);
            }).error(function(error) {
              console.log(error);
              $scope.responseMessage = error.message;
              $scope.showMessage = true;
              $cookieStore.remove('custId');
              $scope.customerId = false;
            });
      }

      $scope.saveCustomerId = function() {
        $cookieStore.put('custId', $scope.loginForm.customerNumber);
        $http.get('http://192.168.2.1:9000/api/engagement/'+ $scope.loginForm.customerNumber)
            .success(function(data) {
              $scope.baseInfo = data;
            }).error(function(error) {
              console.log(error);
              $scope.responseMessage = error.message;
              $scope.showMessage = true;
              $cookieStore.remove('custId');
              $scope.customerId = false;
            });
        $route.reload();
      };




      $scope.removeCookie = function() {
        $cookieStore.remove('custId');
        $route.reload();
      };

    }]);

angular.module('miniBankApp')
    .controller('AccountCtrl', ['$scope', '$routeParams', '$http', '$cookieStore', '$timeout', function ($scope, $routeParams, $http, $cookieStore, $timeout){
      $scope.accountId = $routeParams.accountId;
      $scope.showMessage = false;
      $http.get('http://192.168.2.1:9000/api/transactions/' + $cookieStore.get('custId') + '/' + $scope.accountId)
          .success(function(data) {
            $scope.accountData = data;
            console.log(data);
          }).error(function(error) {
            console.log(error);
            $scope.showMessage = true;
            $scope.responseMessage = error.message;
          });

      $scope.createTransfer = function() {
        var transfer = {
          fromAccount: $scope.accountId,
          toAccount: $scope.transferForm.toAccount,
          amount: $scope.transferForm.transferAmount,
          message: $scope.transferForm.transferMessage
        };

        $http.post('http://192.168.2.1:9000/api/transfer/' + $cookieStore.get('custId'), transfer)
            .success(function(data){
              $scope.transferForm.toAccount = '';
              $scope.transferForm.transferAmount = '';
              $scope.transferForm.transferMessage = '';
              $scope.showMessage = true;
              $scope.responseMessage = "Your transaction from " + data.fromAccount + ' to ' +  data.toAccount + ' is successful';
              $http.get('http://192.168.2.1:9000/api/transactions/' + $cookieStore.get('custId') + '/' + $scope.accountId)
                  .success(function(data) {
                    $scope.accountData = data;
                  }).error(function(error) {
                    $scope.showMessage = true;
                    $scope.responseMessage = error.message;
                  });
            })
            .error(function(error){
              console.log(error);
              $scope.showMessage = true;
              $scope.responseMessage = error.message;
            });
      };
    }]);
