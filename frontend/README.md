Welcome to the Front-End portion of the Mini-Bank
=================================================

This is the AngularJS/Yeoman based front-end application, which uses the REST API, provided by the Play framework backend.

The JAVASCRIPT application uses several running components:

1.  AngularJS by Google - the main frontend Javascript framework.
2.  Yeoman - an application scaffold generator, used to generate the base application components.
3.  Grunt - task runner, used to serve the application files.
4.  Bower - package manager, used to handle the in-app packages
5.  NPM - Node Package Manager, used for initial installation of the prime packages
6.  Node.js - Javascript server, used for Cross Origin Requests and base Request formatting.
7.  GIT command line - version control tool

Installation Instructions
--------------------------

1.  Install git from git-scm.com, don't forget to enable the command line git.
2.  Download Node.js from nodejs.org
3.  Install grunt command line interface - "npm install -g grunt-cli"
4.  Go into the checked out applications /frontend folder in your native terminal application (Command prompt for windows, terminal for UNIX based systems)
5.  Run the "grunt serve" command and the application should start in your default browser under http://localhost:8500

Main parts of the application to take note
-------------------------------------------

1.  index.html - the main scaffold/skeleton of the website. Funnily enough, nothing will be changed here.
2.  /scripts - the folder where the angular javascript components are held - controllers, services and the main application route file.
    2.1. /scripts/controllers - the folder, where the angular controllers are held. Controllers are the primary scope manipulation tool in Angular. DOM (Document Object Model) manipulation is strictly forbidden in the controller.
    2.2. /scrips/services - AngularJS service folder. Services are functions, used for communicating to the backend services, such as REST APIs in big applications. For our example app, we will keep the http requests in the controller, since there is no need to move them out yet.
    2.3. /scripts/app.js - The main route file for the application, which defines the states used in it and the routes used to reach said states.
3.  /styles - the style folder for the application - written in the Cascading Style Sheet (CSS) language. It is usual to use SASS or LESS precompilers for these kinds of applications, but they are not used due to the need to install them.
4.  /views - the view folder. A view is a page of the application, which is pointed to by the routes in app.js in the /scripts folder. The application uses views and their linked controllers to control the display of data on the website. In this application the main.html and about.html views are connected to the controllers in the main.js script file. Usually, when working with bigger applications, every controller is given its separate file.

The POINT of this application
------------------------------

1.  Web application/website building intro.
2.  Basic AngularJS knowledge, learning about {{variables}}, $scopes, controllers, views, routes. Preliminary Javascript knowledge is required.
3.  Introduction to $http requests, using Angular. $http requests are becoming a more and more popular way of working with data and communicating with back-end applications by the way of a RESTful API.



