# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table account (
  id                        integer auto_increment not null,
  number                    varchar(255) not null,
  customer_id               integer,
  constraint uq_account_number unique (number),
  constraint pk_account primary key (id))
;

create table customer (
  id                        integer auto_increment not null,
  number                    varchar(255) not null,
  name                      varchar(255) not null,
  constraint uq_customer_number unique (number),
  constraint pk_customer primary key (id))
;

create table transaction (
  id                        integer auto_increment not null,
  amount                    integer not null,
  date                      timestamp not null,
  to_account                varchar(255) not null,
  from_account              varchar(255) not null,
  sender_name               varchar(255) not null,
  receiver_name             varchar(255) not null,
  message                   varchar(255) not null,
  constraint pk_transaction primary key (id))
;

alter table account add constraint fk_account_customer_1 foreign key (customer_id) references customer (id) on delete restrict on update restrict;
create index ix_account_customer_1 on account (customer_id);



# --- !Downs

SET REFERENTIAL_INTEGRITY FALSE;

drop table if exists account;

drop table if exists customer;

drop table if exists transaction;

SET REFERENTIAL_INTEGRITY TRUE;

